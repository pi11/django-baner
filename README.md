# Simple Django baner app

Uses:

* install baner app - python setup.py install
* add 'baner' app to INSTALLED_APP
* run python manage.py syncdb
* go to admin section and add some baners with unique names
* add "{% load banertag %}" to your template file
* add {% get_baner baner_name %} to your template file
