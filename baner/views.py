# Create your views here.

from django.http import HttpResponse
from django.shortcuts import get_object_or_404

from .models import Baner


def get_baner(request, name):
    baner = get_object_or_404(Baner, name=name)
    return HttpResponse(baner.content, content_type="text/html")

