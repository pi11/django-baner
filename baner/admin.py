# -*- coding: utf-8 -*-

from django.contrib import admin
from baner.models import Baner, IgnoreReferer, IgnoreUserAgent, OnlyReferer

class BanerAdmin(admin.ModelAdmin):
    search_fields = ('name', 'content',)
    list_display = ('name', 'is_active', 'added')

admin.site.register(Baner, BanerAdmin)


class IgnoreRefererAdmin(admin.ModelAdmin):
    search_fields = ('name', )
    list_display = ('name', 'is_active', 'added')

admin.site.register(IgnoreReferer, IgnoreRefererAdmin)

class IgnoreUserAgentAdmin(admin.ModelAdmin):
    search_fields = ('name', )
    list_display = ('name', 'is_active', 'added')

admin.site.register(IgnoreUserAgent, IgnoreUserAgentAdmin)


class OnlyRefererAdmin(admin.ModelAdmin):
    search_fields = ('name', )
    list_display = ('name', 'is_active', 'added')

admin.site.register(OnlyReferer, OnlyRefererAdmin)


