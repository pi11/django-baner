from django.db import models


class Baner(models.Model):
    name = models.CharField(max_length=100, unique=True)
    content = models.TextField(null=True, blank=True)
    is_active = models.BooleanField(default=True)
    added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name

class IgnoreUserAgent(models.Model):
    baner = models.ForeignKey(Baner, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=True)
    is_active = models.BooleanField(default=True)
    added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name


class IgnoreReferer(models.Model):
    baner = models.ForeignKey(Baner, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=True)
    is_active = models.BooleanField(default=True)
    added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    

class OnlyReferer(models.Model):
    baner = models.ForeignKey(Baner, null=True, blank=True, on_delete=models.CASCADE)
    name = models.CharField(max_length=100, unique=True)
    is_active = models.BooleanField(default=True)
    added = models.DateTimeField(auto_now=True)

    def __str__(self):
        return self.name
    
