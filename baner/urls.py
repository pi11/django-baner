from django.urls import include, path

from .views import get_baner

app_name = "baner"

urlpatterns = [
    path(r'get-baner/<str:name>/', get_baner, name='get_baner'),

]
