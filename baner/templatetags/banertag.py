import traceback
from django.template import Library, Node
from django.utils.safestring import mark_safe
from django.db.models import Q

from baner.models import Baner, IgnoreUserAgent, IgnoreReferer, OnlyReferer

register = Library()


def get_baner(parser, token):
    """
    {% get_baner "my_baner" %}
    """
    try:
        tag_name, baner_name = token.split_contents()
    except ValueError:
        raise template.TemplateSyntaxError(
            "%r tag requires a single argument" % token.contents.split()[0]
        )
    return BanerObject(baner_name)


class BanerObject(Node):
    def __init__(self, baner_name):
        self.baner_name = baner_name

    def render(self, context):
        try:
            baner = Baner.objects.get(name=self.baner_name)
        except Baner.DoesNotExist:
            return mark_safe("<!-- baner %s not found -->" % self.baner_name)
        if not baner.is_active:
            return ""
        
        baner_content = mark_safe(baner.content)
        
        ua = context["request"].META.get("HTTP_USER_AGENT", "")
        for iua in IgnoreUserAgent.objects.filter(is_active=True).filter(Q(baner__isnull=True)|Q(baner=baner)):
            try:
                if iua.name:
                    if iua.name.lower() in ua.lower():
                        return ""  # ignore user-agent
            except UnicodeDecodeError:
                return ""  # ignore weird user-agents
            except:
                print(traceback.format_exc())

        ref = context["request"].META.get("HTTP_REFERER", "")
        for iref in IgnoreReferer.objects.filter(is_active=True).filter(Q(baner__isnull=True)|Q(baner=baner)):
            try:
                if iref.name in ref:
                    return ""  # ignore referer
            except:
                print(traceback.format_exc())
                # pass
                
        good = True        
        for iref in OnlyReferer.objects.filter(is_active=True).filter(Q(baner__isnull=True)|Q(baner=baner)):
            try:
                if iref.name in ref:
                    pass
                else:
                    good = False
            except:
                good = False
                print(traceback.format_exc())
                # pass
        if good:
            return baner_content
        else:
            return ""


register.tag("get_baner", get_baner)
