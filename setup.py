from distutils.core import setup

setup(name='django-baner',
      version='0.5',
      description='Very simple django-baner management',
      url='https://bitbucket.org/pi11/django-baner',
      packages=['baner', 'baner.templatetags']
      )
